/******************************************************************************
    GIAN HPCCP-2018
    Question NO : 4 
    Name	: ARRAY INCREMENT
    Objective   : Write a CUDA program to perform ARRAY INCREMENT
    Input	: ARRAY
    Output	: INCREMENTED ARRAY
    
*******************************************************************************/
#include<stdio.h>
#include<cuda.h>

__global__ void incrArrayOnDevice(float *a, int N)
{
int idx = blockIdx.x*blockDim.x + threadIdx.x;  if (idx<N) a[idx] = a[idx]+1.f;
}



main() 
{ 
float *a_h, *a_d;
int i, N=1000;
size_t size = N*sizeof(float); 
a_h = (float *)malloc(size);

//initialize array with dummy values
for (i=0; i<N; i++) a_h[i] = (float)i;

// allocate array on device  
cudaMalloc((void **) &a_d, size);

// copy data from host to device  
cudaMemcpy(a_d, a_h, sizeof(float)*N,cudaMemcpyHostToDevice);

// do calculation on device:
// Part 1 of 2. Compute execution configuration  
int blockSize = 4;
int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

// Part 2 of 2. Call 
//incrementArrayOnDevice kernel  
incrArrayOnDevice <<< nBlocks, blockSize >>> (a_d, N);

// Retrieve result from device and store in b_h
cudaMemcpy(a_h, a_d, sizeof(float)*N, cudaMemcpyDeviceToHost);


//show incremented result
for (i=0; i<N; i++) printf("%f\n",a_h[i]);

// cleanup  
free(a_h);
  
cudaFree(a_d);
}


