/*********************************************************************************************
  GIAN HPCCP-2018
  Question NO   : 11 
  Name		: SHARED MATRIX MULTIPLICATION
  Objective	: Write a CUDA Program to Matrix Multiplication.
  Input		: TWO MATRICES  
  Output	: RESULTANT MATRIX.
 
*********************************************************************************************/
#include<stdio.h>
#include<sys/time.h>
#define TILE_WIDTH 4	//TILE_WIDTH same as block size

__global__ void MatMul(float* A, float* B, float* C, int N)
{
	int k, m;
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
//	__constant__ int TILE_WIDTH = blockDim.x;

	int i = TILE_WIDTH * bx + tx;
	int j = TILE_WIDTH * by + ty;
	__shared__ float As[TILE_WIDTH][TILE_WIDTH], Bs[TILE_WIDTH][TILE_WIDTH]; 

	float sum = 0;
	if(i < N && j < N) {
		for(m = 0; m < N/TILE_WIDTH; m++) {
			As[tx][ty] = A[i*N + (m*TILE_WIDTH + ty)]; 
			Bs[tx][ty] = B[(m*TILE_WIDTH + tx) * N + j]; 
			__syncthreads();

			for(k=0; k<TILE_WIDTH; k++) {
				sum += As[tx][k] * Bs[k][ty];
			}
			__syncthreads();
		}
		C[i*N + j] = sum;
	}
}

int main()
{
	int i, j, N =4;//MAX N==834
	size_t size = N * N * sizeof(float);
	struct timeval tim;  

	// Allocating host and initializing
	float A[N][N],B[N][N],C[N][N];
	for(i=0;i<N;i++) {
		for(j=0;j<N;j++) {
			A[i][j] = B[i][j] =i+j ;
		}
	}
	
	gettimeofday(&tim, NULL);  
	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);  

	// Allocating device and copying to device
	float *d_A, *d_B, *d_C;
	cudaMalloc((void **)&d_A, size);
	cudaMalloc((void **)&d_B, size);
	cudaMalloc((void **)&d_C, size);

	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);

	// Invoking kernel
	dim3 threadsPerBlock(TILE_WIDTH, TILE_WIDTH);
	dim3 blocksPerGrid((N + threadsPerBlock.x - 1) / threadsPerBlock.x, (N + threadsPerBlock.y - 1)/threadsPerBlock.y);

	MatMul<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, N);

	// Copy result from device to host
	cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);

//	for(i=0;i<N;i++)
//		for(j=0;j<N;j++)
//		printf("%f -> %f\n", A[i][j],C[i][j]);
	//printf("%f -> %f\n", A[i-1][j-1],C[i-1][j-1]);

for(i=0;i<N;i++)
        {
		for(j=0;j<N;j++)
                {
		printf("%f ",C[i][j]); 
                }     
	     printf("\n");
        }




	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
	
	gettimeofday(&tim, NULL);  
	double t2=tim.tv_sec+(tim.tv_usec/1000000.0);  
	printf("%f\n", t2 - t1);
}
