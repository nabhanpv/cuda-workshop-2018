/******************************************************************************
    GIAN HPCCP-2018
    Question NO : 3 
    Name	: SINGLE THREAD ADDITION
    Objective   : Write a CUDA program to perform  SINGLE THREAD ADDITION
    Input	: Two Variables
    Output	: Sum of Two Variables
    
*******************************************************************************/
#include<stdio.h>
#include<cuda.h>

__global__ void add(int a,int b,int *c){

 *c=a+b;
}

main()
{
int c;
int *d_c;

// allocate array on device
cudaMalloc((void**)&d_c,sizeof(int));

//<<<X,Y>>> X=number of blocks in a grid, Y=number of threads in a block
//here 1*1 is single threaded function 
add<<<1,1>>>(4,8,d_c);

// copy data from device to host 
cudaMemcpy(&c,d_c, sizeof(int),cudaMemcpyDeviceToHost);

printf("Sum of 4 and 8 = %d\n",c);

// cleanup 
cudaFree(d_c);

}
