/*********************************************************************************************
  GIAN HPCCP-2018
  Question NO   : 8 
  Name		: MATRIX MULTIPLICATION
  Objective	: Write a CUDA Program to perform Matrix multiplication.
  Input		: TWO MATRICES 
  Output	: RESULTANT MATRIX 
 
*********************************************************************************************/
#include<stdio.h>
#include<sys/time.h>
#include<cuda_runtime.h>

__global__ void MatMul(float* A, float* B, float* C, int N)
{
	int i, sum;
	int p = blockDim.x * blockIdx.x + threadIdx.x;
	int q = blockDim.y * blockIdx.y + threadIdx.y;
	if(p < N && q < N) {
		sum = 0;
		for(i=0; i<N; i++) {
			sum += A[p*N + i] * B[i*N + q];
		}
		C[p*N + q] = sum;
	}
}

int main()
{
	int i, j, N =1000;//MAX N==834 
	size_t size = N * N * sizeof(float);
	struct timeval tim;  

	// Allocating host and initializing
	float A[N][N],B[N][N],C[N][N];
	for(i=0;i<N;i++) {
		for(j=0;j<N;j++) {
			A[i][j] = B[i][j] =i+j ;
		}
	}
	gettimeofday(&tim, NULL);  
	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);  

	// Allocating device and copying to device
	float *d_A, *d_B, *d_C;
	cudaMalloc((void **)&d_A, size);
	cudaMalloc((void **)&d_B, size);
	cudaMalloc((void **)&d_C, size);

	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);

	// Invoking kernel
	dim3 threadsPerBlock(8, 8);
	dim3 blocksPerGrid((N + threadsPerBlock.x - 1) / threadsPerBlock.x, (N + threadsPerBlock.y - 1)/threadsPerBlock.y);

	MatMul<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, N);

	// Copy result from device to host
	cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);
        

        //print the resultant Matrix
	for(i=0;i<N;i++)
        {
		for(j=0;j<N;j++)
                {
		printf("%f ",C[i][j]); 
                }     
	     printf("\n");
        }
 

        //cleanup
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
	

	gettimeofday(&tim, NULL);  
	double t2=tim.tv_sec+(tim.tv_usec/1000000.0);  
	printf("%f\n", t2 - t1);
}
