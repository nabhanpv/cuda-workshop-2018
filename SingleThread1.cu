/******************************************************************************
    GIAN HPCCP-2018
    Question NO : 2 
    Name	: SINGLE THREAD
    Objective   : Write a CUDA program to perform SINGLE THREAD
    Input	: None
    Output	: None
    
*******************************************************************************/
#include<stdio.h>
#include<cuda.h>

__global__ void kernel(void){
//empty function that runs on GPU 
}

main()
{
//<<<X,Y>>> X=number of blocks in a grid, Y=number of threads in a block
//here 1*1 is single threaded function 
kernel<<<1,1>>>();
//print something on CPU
printf("HELLO, World!!\n");
}
