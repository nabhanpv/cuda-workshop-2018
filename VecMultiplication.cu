
/******************************************************************************
    GIAN HPCCP-2018
    Question NO : 7 
    Name	: VECTOR MULTIPLICATION
    Objective   : Write a CUDA program to perform VECTOR MULTIPLICATION
    Input	: TWO VECTORS
    Output	: RESULTANT VECTOR
    
*******************************************************************************/
#include<stdio.h>
#include<cuda.h>

__global__ void VecMul(float* A, float* B, float* C, int N)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;

	if(i < N)
		C[i] = A[i]*B[i];
}

int main()
{
	int i, N = 10;
	size_t size = N * sizeof(float);

	// Allocating host and initializing
	float A[N],B[N],C[N];
	for(i=0;i<N;i++) {
		A[i] = B[i] = i;
	}

	// Allocating device and copying to device
	float *d_A, *d_B, *d_C;
	cudaMalloc((void **)&d_A, size);
	cudaMalloc((void **)&d_B, size);
	cudaMalloc((void **)&d_C, size);

	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);

	// Invoking kernel
	int threadsPerBlock = 8;
	int blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;

	VecMul<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, N);

	// Copy result from device to host
	cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);

	for(i=0;i<N;i++)
		printf("%f\n", C[i]);
}
