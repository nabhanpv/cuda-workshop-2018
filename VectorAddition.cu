/******************************************************************************
    GIAN HPCCP-2018
    Question NO : 5 
    Name	: VECTOR ADDITION
    Objective   : Write a CUDA program to perform VECTOR ADDITION
    Input	: TWO VECTORS
    Output	: SUM VECTOR OF GIVEN TWO VECTORS
    
*******************************************************************************/
#include<cuda.h>
#include<stdio.h>
//#include<cuPrintf.cu>

__global__ void VecAdd(float* A, float* B, float* C, int N)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
//	CUPRINTF("%d\n", i);
	if(i < N)
		C[i] = A[i] + B[i];
}

int main()
{
	int i, N = 1000;
	size_t size = N * sizeof(float);

	// Allocating host and initializing
	float A[N],B[N],C[N];
	for(i=0;i<N;i++) {
		A[i] = B[i] = i;
	}

	// Allocating device and copying to device
	float *d_A, *d_B, *d_C;
	cudaMalloc((void **)&d_A, size);
	cudaMalloc((void **)&d_B, size);
	cudaMalloc((void **)&d_C, size);

	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);

	// Invoking kernel
	int threadsPerBlock = 8;
	int blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;

	VecAdd<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, N);

	// Copy result from device to host
	cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);

	for(i=0;i<N;i++)
		printf("%f\n", C[i]);
}
